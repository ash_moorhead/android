package com.spacespud.inform.myimu;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.lab311a.myimu.R;
import com.spacespud.inform.myimu.helpers.SpeedMeter;


public class MyIMU extends Activity implements SensorEventListener, SpeedMeter.SpeedMeterListener{
//TODO save GPS data in a seperate file

    //Get the Sensor Tools
    private SensorManager smanager;
    private Sensor acc, gy, mg, baro;

    private long startTime = 0;
    private int count;
    private float firstPressure = 0;
    private boolean isConnected=false;

    //File writing utility
    private File file;
    private PrintWriter writer;
    private SpeedMeter mSpeedMeter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_imu);

        //get the SensorManager.
        smanager=(SensorManager) getSystemService(SENSOR_SERVICE);

        //Get the IMU Sensors.
        acc= smanager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mg=smanager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);			//This will give magnetometer data.
        gy=smanager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        baro = smanager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        mSpeedMeter = new SpeedMeter(this, this, SpeedMeter.SpeedOption.KMPH);

    }

    private void init_file() {
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/myimu");

        try {
            if (!dir.exists()) {
                if(!dir.mkdirs()){
                    throw new Exception("Could not create folder");
                }
            }
            file = new File(dir, ""+date+"_"+month+" "+hour+":"+minute+":"+second+".txt");
            writer = new PrintWriter(new FileWriter(file));
        } catch (Exception ex) {
            Toast.makeText(this, "Can't Open File", Toast.LENGTH_LONG).show();
        }
    }


    public void btn_clicked(View view) {
        Button button=(Button) view;

        if(button.getText().equals("Start")) {

            init_file();
            count = 0;
            mSpeedMeter.connect();
            button.setText(R.string.button_stop);
            Toast.makeText(this, "File: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();

        }else {
            //unregister the sensors.
            smanager.unregisterListener(this);
            button.setText(R.string.button_start);
        }

    }

    private void StartDataRecording() {
        //register sensors
        smanager.registerListener(this, acc, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, mg, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, gy, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, baro, SensorManager.SENSOR_DELAY_FASTEST);
        startTime = System.currentTimeMillis();

    }

    @Override
    public void onDestroy() {
        try {
            smanager.unregisterListener(this);
            writer.close();
            if (mSpeedMeter.isConnected())
                mSpeedMeter.disconnect();
        }catch(Exception ex) {
            System.out.print("could not close file");
        }
        super.onDestroy();

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // do nothing
    }

    public void save( String ax) {

        PrintWriter outFile = null;
        try {

            outFile = new PrintWriter(new FileWriter(file.getAbsolutePath(), true));
            outFile.print(ax);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /*
     * This is the sensor reading callback. Android system push the sensor data here when they are ready.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {

        double seconds = System.currentTimeMillis() - startTime;

        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER) {
            double ax=event.values[0];
            double ay=event.values[1];
            double az=event.values[2];

            String ot = "A" + seconds + "\t" + ax + "\t" + ay + "\t" + az  + "\r\n";
            save(ot);
        }else if(event.sensor.getType()==Sensor.TYPE_GYROSCOPE) {
            double gx=event.values[0];
            double gy=event.values[1];
            double gz=event.values[2];

            String ot = "G" + seconds + "\t" + gx + "\t" + gy + "\t" + gz  + "\r\n";
            save(ot);

        }else if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD) {
            float Z = event.values[0];
            float X = event.values[1];
            float Y = event.values[2];

            String ot = "M" + seconds + "\t" + Z + "\t" + X + "\t" + Y  + "\r\n";
            save(ot);

        } else if (event.sensor.getType()==Sensor.TYPE_PRESSURE){

            if (count < 5){
                firstPressure += event.values[0];
                count++;
            }
            else {
                firstPressure = firstPressure/5;
                float pressure = event.values[0];
                //TODO make a dynamic constant
                float height = ((pressure - firstPressure)*100) / (-1.1799f * 9.81f);

                String ot = "B" + seconds + "\t" + pressure + "\t" + height + "\t" + height + "\r\n";
                save(ot);
            }

        }
    }

    public void onConnected(Bundle bundle){
        StartDataRecording();
        Toast.makeText(this, "GPS Connection Made!! Data Logging", Toast.LENGTH_LONG).show();
    }

    public void onConnectionFailed(ConnectionResult result){
        Toast.makeText(this, "GPS Connection Failed 8===D", Toast.LENGTH_LONG).show();
    }

    public void onSpeedChange(double speed){

        double seconds = System.currentTimeMillis() - startTime;

        String ot = "S" + seconds + "\t" + speed + "\t" + speed + "\t" + speed  + "\r\n";
        save(ot);
    }
}


