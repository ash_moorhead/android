package shakasoftware.engine2.capture;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

import shakasoftware.engine2.R;
import shakasoftware.engine2.data.DataLogging;

/**
 * Created by benjaminroe on 11/02/16.
 */

public class CaptureActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback {
    static final int REQUEST_VIDEO_CAPTURE = 1;
    static final int REQUEST_VIDEO_PLAY = 2;
    private boolean isRecording = false;
    private boolean useCamera = true;
    private boolean previewRunning = false;
    private String temp;
    private String filepath;
    private Uri mUriPath;

    private Button mRecordButton;
    private Button mPlaybackButton;
    private SeekBar mSeekbar;
    private ImageView mRecordingProgress;
    private Handler mHandler = new Handler();
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private SurfaceHolder mHolder;
    private CamcorderProfile mCamcorderProfile;
    private Camera mCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        DataLogging.INSTANCE.setContext(this);

        mCamcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        //mRecordingProgress = (ImageView) findViewById(R.id.recording_progress_button);
        mPlaybackButton = (Button) findViewById(R.id.playback_button);
        mPlaybackButton.setOnClickListener(this);
        mRecordButton = (Button) findViewById(R.id.record_button);
        mRecordButton.setOnClickListener(this);

        SurfaceView cameraView = (SurfaceView) findViewById(R.id.camera_view);
        mHolder = cameraView.getHolder();
        mHolder.addCallback(this);
        mPlayer = new MediaPlayer();
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mPlayer.start();
            }
        });

        final Context context = getApplicationContext();
        final GestureDetector.SimpleOnGestureListener listener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if(isRecording) {
                    DataLogging.INSTANCE.stop();
                    Toast.makeText(context, "STOPPED", Toast.LENGTH_SHORT).show();
                    stopRecorder();
                }
                return true;
            }
        };
        final GestureDetector detector = new GestureDetector(listener);
        detector.setOnDoubleTapListener(listener);
        getWindow().getDecorView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return detector.onTouchEvent(event);
            }
        });
    }
    private void prepareRecorder() {
        mRecorder = new MediaRecorder();
        mRecorder.setPreviewDisplay(mHolder.getSurface());

        if (useCamera) {
            mCamera.unlock();
            mRecorder.setCamera(mCamera);
        }

        mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

        mRecorder.setProfile(mCamcorderProfile);

        try {
            File newFile = File.createTempFile("videocapture", ".mp4", Environment.getExternalStorageDirectory());
            mRecorder.setOutputFile(newFile.getAbsolutePath());
            if(temp == null) {
                filepath = newFile.getAbsolutePath();
            } else filepath = temp;
            //Reinstantiate the below as these are being recreated before recorded over
            temp = newFile.getAbsolutePath();
            //mUriPath = Uri.fromFile(newFile.getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
        //recorder.setMaxDuration(50000); // 50 seconds
        //recorder.setMaxFileSize(5000000); // Approximately 5 megabytes

        try {
            mRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            finish();
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
    }

    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.record_button:
                startDataLogging();
                isRecording = true;
                mRecorder.start();
                Toast.makeText(this, "STARTED", Toast.LENGTH_LONG).show();
                //mRecordingProgress.setVisibility(View.VISIBLE);
                mRecordButton.setVisibility(View.GONE);
                break;
            case R.id.playback_button:
                try {
                    FileDescriptor fd = null;
                    FileInputStream fis = new FileInputStream(filepath);
                    fd = fis.getFD();
                    mPlayer.reset();
                    mPlayer.setDataSource(fd);
                    mPlayer.setDisplay(mHolder);
                    //mPlayer.prepareAsync();
                    mPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (useCamera) {
            mCamera = Camera.open();


            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
                mPlayer.setDisplay(holder);
                previewRunning = true;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (!isRecording && useCamera) {
            if (previewRunning){
                mCamera.stopPreview();
            }

            try {
                Camera.Parameters p = mCamera.getParameters();

                p.setPreviewSize(mCamcorderProfile.videoFrameWidth, mCamcorderProfile.videoFrameHeight);
                p.setPreviewFrameRate(mCamcorderProfile.videoFrameRate);

                mCamera.setParameters(p);

                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
                previewRunning = true;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            prepareRecorder();
        }
    }


    public void surfaceDestroyed(SurfaceHolder holder) {
        if (isRecording) {
            mRecorder.stop();
            isRecording = false;
        }
        mRecorder.release();
        if (useCamera) {
            previewRunning = false;
            //mCamera.lock();
            mCamera.release();
        }
        //finish();
    }

    private void stopRecorder(){
        mRecordButton.setVisibility(View.GONE);
        findViewById(R.id.recording_screen).setVisibility(View.GONE);
        findViewById(R.id.pause_screen).setVisibility(View.VISIBLE);
        mRecorder.stop();
        if (useCamera) {
            try {
                mCamera.reconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        DataLogging.INSTANCE.stop();
        mRecorder.release();
        isRecording = false;
        Toast.makeText(this, "STOPPED", Toast.LENGTH_LONG).show();
        //mRecordingProgress.setVisibility(View.INVISIBLE);
        //mRecordButton.setVisibility(View.VISIBLE);
        //mPlaybackButton.setVisibility(View.VISIBLE);
        prepareRecorder();
    }

    private void startDataLogging() {
        DataLogging.INSTANCE.startDataRecording();
        DataLogging.INSTANCE.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mPlayer != null) mPlayer.release();
        if(mCamera != null) mCamera.release();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            if(isRecording) {
                stopRecorder();
                //createSeeker();
            } else {
                mRecordButton.setVisibility(View.GONE);
                findViewById(R.id.recording_screen).setVisibility(View.VISIBLE);
                findViewById(R.id.pause_screen).setVisibility(View.GONE);
                isRecording = true;
                startDataLogging();
                mRecorder.start();
                Toast.makeText(this, "STARTED", Toast.LENGTH_LONG).show();
            }
        }
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            if(isRecording) {
                stopRecorder();
                //createSeeker();
            } else {
                mRecordButton.setVisibility(View.GONE);
                findViewById(R.id.recording_screen).setVisibility(View.VISIBLE);
                findViewById(R.id.pause_screen).setVisibility(View.GONE);
                isRecording = true;
                startDataLogging();
                mRecorder.start();
                Toast.makeText(this, "STARTED", Toast.LENGTH_LONG).show();
            }
        }
        return true;
    }

    private void createSeeker() {
        mSeekbar = (SeekBar) findViewById(R.id.pause_screen).findViewById(R.id.pauseLayout).findViewById(R.id.myseek);
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (seekBar.getProgress() > 95) {

                } else {
                    seekBar.setThumb(getResources().getDrawable(R.drawable.heart));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if(progress>95){
                    seekBar.setThumb(getResources().getDrawable(R.drawable.ic_full_speedo));
                }

            }
        });
    }
}
