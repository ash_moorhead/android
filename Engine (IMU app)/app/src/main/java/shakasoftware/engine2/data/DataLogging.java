package shakasoftware.engine2.data;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

/**
 * Created by benjaminroe on 16/03/16.
 */
public enum DataLogging implements SensorEventListener, SpeedMeter.SpeedMeterListener {
    INSTANCE;
    private SensorManager smanager;
    private Sensor acc, gy, mg, baro;
    private long startTime = 0;
    private int count;
    private float firstPressure = 0;
    private boolean isConnected = false;
    private Context mContext;
    //File writing utility
    private File file;
    private PrintWriter writer;
    private SpeedMeter mSpeedMeter;

    public void setContext(Context context) {
        if(context != null) {
            mContext = context;
        }
    }

    private void initFile() {
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/myimu");
        try {
            if (!dir.exists()) {
                if(!dir.mkdirs()){
                    throw new Exception("Could not create folder");
                }
            }
            file = new File(dir, ""+date+"_"+month+" "+hour+":"+minute+":"+second+".txt");
            writer = new PrintWriter(new FileWriter(file));
        } catch (Exception ex) {
            Toast.makeText(mContext.getApplicationContext(), "Can't Open File", Toast.LENGTH_SHORT).show();
        }
    }

    public void start(){
        initFile();
        count = 0;
        if(mSpeedMeter.isConnected()){
            mSpeedMeter.resume();
        } else{
            mSpeedMeter.connect();
        }
        Toast.makeText(mContext.getApplicationContext(), "File: " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
    }

    public void stop() {
        if (mSpeedMeter.isConnected()) {
            mSpeedMeter.pause();
        }
        smanager.unregisterListener(this);
    }

    public void startDataRecording() {
        //get the SensorManager.
        smanager= (SensorManager) mContext.getSystemService(mContext.SENSOR_SERVICE);
        //Get the IMU Sensors.
        acc= smanager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mg=smanager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);			//This will give magnetometer data.
        gy=smanager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        baro = smanager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        //register sensors
        smanager.registerListener(this, acc, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, mg, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, gy, SensorManager.SENSOR_DELAY_GAME);
        smanager.registerListener(this, baro, SensorManager.SENSOR_DELAY_FASTEST);
        startTime = System.currentTimeMillis();
        mSpeedMeter = new SpeedMeter(mContext, this, SpeedMeter.SpeedOption.KMPH);
    }

    public void destroy() {
        try {
            smanager.unregisterListener(this);
            writer.close();
            if (mSpeedMeter.isConnected())
                mSpeedMeter.disconnect();
        }catch(Exception ex) {
            System.out.print("could not close file");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // do nothing
    }

    public void save( String ax) {
        PrintWriter outFile = null;
        try {
            outFile = new PrintWriter(new FileWriter(file.getAbsolutePath(), true));
            outFile.print(ax);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (outFile != null) {
                try {
                    outFile.flush();
                    outFile.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // if(event.sensor.accuracy == Sensor.SENSOR_STATUS_UNRELIABLE) { return; }
        //put this shit in an async task?
        double seconds = System.currentTimeMillis() - startTime;
        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER) {
            double ax=event.values[0];
            double ay=event.values[1];
            double az=event.values[2];
            String ot = "A" + seconds + "\t" + ax + "\t" + ay + "\t" + az  + "\r\n";
            save(ot);
        }else if(event.sensor.getType()==Sensor.TYPE_GYROSCOPE) {
            double gx=event.values[0];
            double gy=event.values[1];
            double gz=event.values[2];
            String ot = "G" + seconds + "\t" + gx + "\t" + gy + "\t" + gz  + "\r\n";
            save(ot);
        }else if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD) {
            float Z = event.values[0];
            float X = event.values[1];
            float Y = event.values[2];
            String ot = "M" + seconds + "\t" + Z + "\t" + X + "\t" + Y  + "\r\n";
            save(ot);
        } else if (event.sensor.getType()==Sensor.TYPE_PRESSURE){
            if (count < 5){
                firstPressure += event.values[0];
                count++;
            }
            else {
                firstPressure = firstPressure/5;
                float pressure = event.values[0];
                //TODO make a dynamic constant
                float height = ((pressure - firstPressure)*100) / (-1.1799f * 9.81f);
                String ot = "B" + seconds + "\t" + pressure + "\t" + height + "\t" + height + "\r\n";
                save(ot);
            }
        }
    }

    public void onConnected(Bundle bundle) {
        startDataRecording();
        Toast.makeText(mContext.getApplicationContext(), "GPS Connection Made!! Data Logging", Toast.LENGTH_SHORT).show();
    }

    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(mContext.getApplicationContext(), "GPS Connection Failed 8===D", Toast.LENGTH_SHORT).show();
        Beep();
    }

    public void onSpeedChange(double speed){
        double seconds = System.currentTimeMillis() - startTime;
        String ot = "S" + seconds + "\t" + speed + "\t" + speed + "\t" + speed  + "\r\n";
        save(ot);
        Log.d("SPEED", "" + ot);
    }

    private void Beep(){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(mContext.getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
