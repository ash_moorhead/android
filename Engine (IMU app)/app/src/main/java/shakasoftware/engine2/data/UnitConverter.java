package shakasoftware.engine2.data;

import android.util.Log;

/**
 * Created by benjaminroe on 21/02/16.
 */
public class UnitConverter {

    public static double meterToKm(double meter) {
        double km = meter / 1000;
        km = (Math.round(km * 100.0) / 100.0);
        Log.e("meter to km", meter + " to " + km);
        return km;
    }

    public static double meterToMile(double meter) {
        double mile = meter / 1609.3;
        mile = (Math.round(mile * 100.0) / 100.0);
        return mile;
    }

    public static double toKmPH(double MPS) {
        double kmph = MPS * 3.6; // convert to KMPH
        kmph = (Math.round(kmph * 100.0) / 100.0);
        return kmph;
    }

    public static double toMiPH(double MPS) {
        double miph = MPS * 2.23694; // convert to KMPH
        miph = (Math.round(miph * 100.0) / 100.0);
        return miph;
    }

    public static String formatTime(long seconds){
        long hour = 0, minute = 0, second = 0;
        if (seconds >= 60) {
            minute = seconds / 60;
            second = seconds % 60;
            if (minute >= 60) {
                hour = minute / 60;
                minute = minute % 60;
            }
        } else {
            hour = minute = 0;
            second = seconds;
        }
        return hour+":"+minute+":"+second;
    }
}