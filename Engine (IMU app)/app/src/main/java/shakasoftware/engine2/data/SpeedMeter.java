package shakasoftware.engine2.data;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by benjaminroe on 21/02/16.
 */
public class SpeedMeter implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private SpeedMeterListener listener;
    private Context context;
    private Location mCurrentLocation, mLastLocation;
    private SpeedOption speedOption;
    private Timer timer;
    private static final int TIME_FRACTION = 1;
    private long time;
    private double maxSpeed;
    private double totalDistance;
    private double di, df, dc;


    //speed meter listener. implement this listener from activity
    public interface SpeedMeterListener {

        void onConnected(Bundle bundle);

        void onConnectionFailed(ConnectionResult result);

        void onSpeedChange(double speed);
    }

    //speed options {KMPH = kilometer per hour, MIPH = mile per hour}
    public enum SpeedOption {
        KMPH, MIPH;
    }

    //initialize all resources at constructor
    //speed listener is essential should not be null
    public SpeedMeter(Context context, SpeedMeterListener listener, SpeedOption option) {
        if (listener == null) {
            throw new NullPointerException("Initialize listener");
        }
        this.context = context;
        this.listener = listener;
        speedOption = option;

        // default speed option is KMPH
        if (speedOption == null)
            speedOption = SpeedOption.KMPH;

        //initialize timer for fractional calculation
        timer = new Timer();

        //builds google api client
        buildGoogleApiClient();
        createLocationRequest();
    }

    //connects speed meter to google api clients.
    public void connect() {
        dc = di = df = totalDistance = 0;
        time = 0;
        maxSpeed = 0;
        mLastLocation = null;
        mCurrentLocation = null;

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        else
            throw new IllegalStateException("google service api not initialize properly");
    }

    //code for pause speed meter which stops listening to location.
    public void pause() {
        timer.cancel();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        } else
            throw new IllegalStateException("google service api did not initialize properly");
    }

    //code for resume speed meter which start listening to location and starts timer
    public void resume() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            onConnected(null);
        } else
            throw new IllegalStateException("google service api not initialize properly");
    }

    //check whether google api client connected
    public boolean isConnected() {
        return mGoogleApiClient != null ? mGoogleApiClient.isConnected() : false;
    }

    //disconnect speed meter and release resources
    public void disconnect() {
        try {
            pause();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        timer.cancel();
    }

    //returns total distance travelled from start to stop;
    public double getDistanceCovered() {
        if (speedOption == SpeedOption.KMPH)
            return UnitConverter.meterToKm(totalDistance);
        else if (speedOption == SpeedOption.MIPH)
            return UnitConverter.meterToMile(totalDistance);

        return 0;
    }

    //return duration in milliseconds
    public long getDuration() {
        return time;
    }

    //returns max speed
    public double getMaxSpeed() {
        if (speedOption == SpeedOption.KMPH)
            return UnitConverter.toKmPH(maxSpeed);
        else if (speedOption == SpeedOption.MIPH)
            return UnitConverter.toMiPH(maxSpeed);

        return 0;
    }

    //callback method of google api client
    @Override
    public void onConnected(Bundle bundle) {
        listener.onConnected(bundle);

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            timer = new Timer();
            //calculate distance change at specific interval
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    time += TIME_FRACTION;
                    if (df != 0) {
                        dc = df - di;
                        Log.e("Change In Distance", "DC : " + dc);
                        //return if drastic change in distance because of gps problem
                        if (dc > 250)
                            return;

                        // Unit: meter per second
                        double currentSpeed = dc / TIME_FRACTION;
                        if (currentSpeed < 0)
                            currentSpeed = currentSpeed * -1;
                        di = df;

                        if (currentSpeed > maxSpeed) {
                            maxSpeed = currentSpeed;
                        }

                        if (speedOption == SpeedOption.KMPH) {
                            final double kmph = UnitConverter.toKmPH(currentSpeed);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onSpeedChange(kmph);
                                }
                            });

                        } else if (speedOption == SpeedOption.MIPH) {
                            final double miph = UnitConverter.toMiPH(currentSpeed);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onSpeedChange(miph);
                                }
                            });
                        }
                    }

                }
            }, 0, TIME_FRACTION * 1000);
        } else
            throw new IllegalStateException("Google service api not connected yet");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    //location change call back method
    //measure total distance covered from here
    @Override
    public void onLocationChanged(Location location) {
//        mCurrentLocation = location;
        if (mCurrentLocation == null) {
            mCurrentLocation = location;
        } else {
            mLastLocation = location;
            totalDistance += calculateDistance(mCurrentLocation, mLastLocation);
            if (di == 0) {
                di = totalDistance;
            } else {
                df = totalDistance;
            }
            mCurrentLocation = mLastLocation;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        listener.onConnectionFailed(connectionResult);
    }

    Handler handler = new Handler();


    //stops receiving location update
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    //starts receiving location updates
    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    //create location as per requirements
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(200);
        mLocationRequest.setFastestInterval(200);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //create google api instance
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    /**
     * calculate distance between two location points returns distance in meter
     */

    public double calculateDistance(Location lStart, Location lEnd) {
        double lat1 = lStart.getLatitude();
        double lat2 = lEnd.getLatitude();
        double long1 = lStart.getLongitude();
        double long2 = lEnd.getLongitude();

        return calculate(lat1, lat2, long1, long2);
    }


    /**
     * calculate distance between two LatLng points returns distance in meter
     */
    public double calculateDistance(LatLng lStart, LatLng lEnd) {
        double lat1 = lStart.latitude;
        double lat2 = lEnd.latitude;
        double long1 = lStart.longitude;
        double long2 = lEnd.longitude;

        return calculate(lat1, lat2, long1, long2);

    }

    private double calculate(double lat1, double lat2, double long1,
                             double long2) {
        double toRad = Math.PI / 180;
        double distance = 0;

        try {
            double dlong = (long2 - long1) * toRad;
            double dlat = (lat2 - lat1) * toRad;
            double a = Math.pow(Math.sin(dlat / 2.0), 2)
                    + Math.cos(lat1 * toRad) * Math.cos(lat2 * toRad)
                    * Math.pow(Math.sin(dlong / 2.0), 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            distance = 6367 * c;

            return distance * 1000;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return distance;
    }
}