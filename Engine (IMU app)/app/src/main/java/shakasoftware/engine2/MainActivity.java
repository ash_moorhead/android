package shakasoftware.engine2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;

import shakasoftware.engine2.capture.CaptureActivity;
import shakasoftware.engine2.data.SpeedMeter;
import shakasoftware.engine2.helpers.VideoFragment;
import shakasoftware.engine2.metric.SpeedFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SpeedMeter.SpeedMeterListener {

    private SpeedMeter mSpeedMeter;
    private boolean hasCameraPermission = false;
    private boolean hasAudioPermission = false;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initVideo();
        mSpeedMeter = new SpeedMeter(this, this, SpeedMeter.SpeedOption.KMPH);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        findViewById(R.id.actionbar).findViewById(R.id.ab_record).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent captureIntent = new Intent(MainActivity.this, CaptureActivity.class);
                startActivity(captureIntent);
//                if (hasAudioPermission && hasCameraPermission) {
//                    Intent captureIntent = new Intent(MainActivity.this, CaptureActivity.class);
//                    startActivity(captureIntent);
//                } else {
//                    checkAudioPermission();
//                    checkCameraPermission();
//                }
            }
        });
    }

    private void initVideo() {
        Fragment f = new VideoFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.flContent, f);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {
            Fragment f = new SpeedFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.addToBackStack(null);
            transaction.add(R.id.flContent, f);
            transaction.commit();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSpeedChange(double speed) {
        //tvInfo.setText(UnitConverter.formatTime(mSpeedMeter.getDuration()) + "\n\nDistance Covered: " + mSpeedMeter.getDistanceCovered() + " KM \n\nSpeed: " + speed + " KMPH" + "\n\nTop Speed: " + mSpeedMeter.getMaxSpeed() + " KMPH");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSpeedMeter.isConnected())
            mSpeedMeter.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        //tvInfo.setText("Failed to connect");
    }

    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission_group.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission_group.CAMERA},
//                        MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission_group.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        } else hasCameraPermission = true;
    }

    private void checkAudioPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission_group.MICROPHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission_group.MICROPHONE},
//                        MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission_group.MICROPHONE},
                        MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
            }
        } else hasAudioPermission = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    hasCameraPermission = true;
                } else {
                    hasCameraPermission = false;
                }
            }
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    hasAudioPermission = true;
                } else {
                    hasAudioPermission = false;
                }
            }
        }
    }
}
