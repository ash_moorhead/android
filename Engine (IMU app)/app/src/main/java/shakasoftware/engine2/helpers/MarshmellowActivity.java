package shakasoftware.engine2.helpers;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import shakasoftware.engine2.R;
import shakasoftware.engine2.metric.MetricsActivity;

/**
 * Created by benjaminroe on 26/02/16.
 */
public class MarshmellowActivity extends Activity {
    private VideoView mVideo;

    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        //mVideo = (VideoView) findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.vid1;
        startVideo(path);
        mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer vmp) {
                Intent intent = new Intent(MarshmellowActivity.this, MetricsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public void onLongPress(MotionEvent e) {
            Toast.makeText(getApplicationContext(), "User is holding the screen, do animation here", Toast.LENGTH_LONG).show();
            //findViewById(R.id.height_metric).setVisibility(View.VISIBLE);
            findViewById(R.id.speed_metric).setVisibility(View.VISIBLE);
        }
        public void onRelease(MotionEvent e) {
            //findViewById(R.id.height_metric).setVisibility(View.INVISIBLE);
            findViewById(R.id.speed_metric).setVisibility(View.INVISIBLE);
        }
    });

    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                //findViewById(R.id.height_metric).setVisibility(View.VISIBLE);
                findViewById(R.id.speed_metric).setVisibility(View.VISIBLE);
                break;
            case MotionEvent.ACTION_UP:
                //findViewById(R.id.height_metric).setVisibility(View.INVISIBLE);
                findViewById(R.id.speed_metric).setVisibility(View.INVISIBLE);
                break;
        }
        return gestureDetector.onTouchEvent(event);
    };

    private void startVideo(String url) {
        try {
            // Start the MediaController
//            MediaController mediacontroller = new MediaController(
//                    this);
//            mediacontroller.setAnchorView(mVideo);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(url);
            //mVideo.setMediaController(mediacontroller);
            mVideo.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        mVideo.requestFocus();
        mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                //mp.setPlaybackParams(new PlaybackParams().setSpeed(0.5f));
                mVideo.start();
            }
        });
    }
}
