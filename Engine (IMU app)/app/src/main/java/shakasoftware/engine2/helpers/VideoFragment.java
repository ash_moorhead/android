package shakasoftware.engine2.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.media.MediaPlayer;

import java.io.IOException;

import shakasoftware.engine2.R;

/**
 * Created by benjaminroe on 21/02/16.
 */
public class VideoFragment extends Fragment {
    View myView;

    public interface Callback {
        void swapFragment(Fragment f);
    }

    private static Callback mDummyCallback = new Callback() {
        @Override
        public void swapFragment(Fragment f) {}

    };

    private Callback mCallback = mDummyCallback;

    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //myView = inflater.inflate(R.layout.fragment_video, container, false);
        myView = inflater.inflate(R.layout.layout_video_tile, container, false);
        ImageView thumbnail = (ImageView) myView.findViewById(R.id.thumbnail);
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VideoActivity.class);
                intent.putExtra("POS", 1);
                getActivity().startActivity(intent);
            }
        });
        ImageView thumbnail1 = (ImageView) myView.findViewById(R.id.thumbnail1);
        thumbnail1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VideoActivity.class);
                intent.putExtra("POS", 2);
                getActivity().startActivity(intent);
            }
        });
        ImageView thumbnail2 = (ImageView) myView.findViewById(R.id.thumbnail2);
        thumbnail2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VideoActivity.class);
                intent.putExtra("POS", 3);
                getActivity().startActivity(intent);
            }
        });
        ImageView thumbnail3 = (ImageView) myView.findViewById(R.id.thumbnail3);
        thumbnail3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VideoActivity.class);
                intent.putExtra("POS", 4);
                getActivity().startActivity(intent);
            }
        });
        return myView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            mCallback = (Callback) context;
        }
    }

    @Override
    public void onDetach() {
        mCallback = mDummyCallback;
        super.onDetach();
    }
}
