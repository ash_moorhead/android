package shakasoftware.engine2.metric;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import shakasoftware.engine2.R;
import shakasoftware.engine2.helpers.VideoFragment;

/**
 * Created by benjaminroe on 11/02/16.
 */
public class MetricsActivity extends AppCompatActivity implements SpeedFragment.Callback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics);
        initMetric();
    }

    private void initMetric() {
        Fragment f = new SpeedFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.metricContent, f);
        transaction.commit();
    }

    @Override
    public void swapFragment(Fragment f) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.metricContent, f);
        transaction.commit();
    }

}
