package shakasoftware.engine2.metric;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import shakasoftware.engine2.R;

/**
 * Created by benjaminroe on 11/02/16.
 */
public class JumpFragment extends Fragment {
    View myView;

    public interface Callback {
        void swapFragment(Fragment f);
    }

    private static Callback mDummyCallback = new Callback() {
        @Override
        public void swapFragment(Fragment f) {}

    };

    private Callback mCallback = mDummyCallback;

    public static JumpFragment newInstance() {
        return new JumpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_jump, container, false);
        return myView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            mCallback = (Callback) context;
        }
    }

    @Override
    public void onDetach() {
        mCallback = mDummyCallback;
        super.onDetach();
    }
}
